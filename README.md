These are drafts of an email signature for Paritie Dynamic Enterprise Limited. You are expected to use them as guide to reproduce at least three (3) samples for staff and faceless company related accounts

Deliverables:

- A dsiclaimer message
- Adequate colour for social media icons
- Readable font in line with the company's design guide
- Position fields for staff 
